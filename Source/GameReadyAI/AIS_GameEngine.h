// Copyright 2015 Matthias Hölzl, All Rights Reserved.

#pragma once

#include "Engine/GameEngine.h"
#include "AIS_DataManager.h"
#include "AIS_DataManagerProvider.h"
#include "AIS_GameEngine.generated.h"

/**
 * A Custom GameEngine to manage persistent data.
 */
UCLASS()
class GAMEREADYAI_API UAIS_GameEngine : public UGameEngine, public IAIS_DataManagerProvider
{
	GENERATED_BODY()
	
public:

	UAIS_GameEngine(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get());

	UAIS_DataManager* GetDataManager() override { return DataManager; };

protected:

	UPROPERTY()
	class UAIS_DataManager* DataManager;
};
