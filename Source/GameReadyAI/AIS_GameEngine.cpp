// Copyright 2015 Matthias Hölzl, All Rights Reserved.

#include "GameReadyAI.h"
#include "AIS_DataManager.h"
#include "AIS_GameEngine.h"

UAIS_GameEngine::UAIS_GameEngine(const FObjectInitializer& ObjectInitializer /*= FObjectInitializer::Get()*/) :
	Super(ObjectInitializer)
{
	DataManager = CreateDefaultSubobject<UAIS_DataManager>(TEXT("DataManager"));
	check(DataManager != nullptr && DataManager->IsValidLowLevel());
}
