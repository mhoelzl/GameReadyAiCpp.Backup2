// Copyright 2016 Matthias Hölzl, All Rights Reserved.

#include "GameReadyAI.h"
#include "CompositeItemEffect.h"
#include "Interfaces/IsUsable.h"
#include "Interfaces/ItemUser.h"

float UCompositeItemEffect::EstimateEffect(const AActor* Item, 	const AActor* User) const
{
	float Result{ 0.0f };
	
	for (UItemEffect* Effect : SharedEffects)
	{
		Result += Effect->EstimateEffect(Item, User);
	}

	for (UItemEffect* Effect : EstimationOnlyEffects)
	{
		Result += Effect->EstimateEffect(Item, User);
	}
	return Result;
}

float UCompositeItemEffect::ApplyEffect(const AActor* Item, AActor* User)
{
	float Result{ 0.0f };

	for (UItemEffect* Effect : SharedEffects)
	{
		Result += Effect->ApplyEffect(Item, User);
	}

	for (UItemEffect* Effect : SharedEffects)
	{
		Result += Effect->ApplyEffect(Item, User);
	}
	return Result;
}

void UCompositeItemEffect::Initialize(FName StatName, float StatDelta)
{
	SharedEffects.Empty(1);
	EstimationOnlyEffects.Empty();
	ApplicationOnlyEffects.Empty();

	SharedEffects.Emplace();
	SharedEffects[0]->Initialize(StatName, StatDelta);
}
