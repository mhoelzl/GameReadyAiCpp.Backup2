// Copyright 2016 Matthias Hölzl, All Rights Reserved.

#pragma once

#include "Interfaces/ItemUser.h"
#include "ItemEffect.generated.h"

/**
 * The effect an item has on a stat.
 */
UCLASS(Blueprintable, BlueprintType, EditInlineNew)
class GAMEREADYAI_API UItemEffect : public UObject
{
	GENERATED_BODY()

public:	
	// Estimate the effect that applying Item would have on User.
	// Return value is the change in utility.
	UFUNCTION(BlueprintPure, Category = "Stats")
	virtual float EstimateEffect(const AActor* /* IIsUsable */ Item, const AActor* /* IItemUser */ User) const;

	// Apply the effect provided by Item to User.
	// We pass in both Item and User so that we can define effects that vary 
	// Return value is the change in utility.
	UFUNCTION(BlueprintCallable, Category = "Stats")
	virtual float ApplyEffect(const AActor* /* IIsUsable */ Item, AActor* /* IItemUser */ User);

	// Ensures that this item effect influences StatName by StatDelta and has
	// no other effects.
	UFUNCTION(BlueprintCallable, Category = "Stats")
	virtual void Initialize(FName StatName, float StatDelta);
};
