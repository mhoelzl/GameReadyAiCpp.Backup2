// Copyright 2016 Matthias Hölzl, All Rights Reserved.

#include "GameReadyAI.h"
#include "SimpleItemEffect.h"
#include "Interfaces/IsUsable.h"
#include "Interfaces/ItemUser.h"
#include "Stats/Stat.h"




float USimpleItemEffect::EstimateEffect(const AActor* Item,	const AActor* User) const
{
	TArray<UStat*> Stats{ IItemUser::Execute_GetAllStats(User) };
	for (UStat* Stat : Stats)
	{
		if (Stat->GetStatName() == StatName)
		{
			return Stat->DeltaUtility(StatDelta);
		}
	}
	return 0.0f;
}

float USimpleItemEffect::ApplyEffect(const AActor* Item, AActor* User)
{
	return IItemUser::Execute_AddToCurrentStatValue(User, StatName, StatDelta);
}

void USimpleItemEffect::Initialize(FName StatName, float StatDelta)
{
	this->StatName = StatName;
	this->StatDelta = StatDelta;
}
