// Copyright 2015, 2016 Matthias Hölzl, All Rights Reserved.

#pragma once

#include "Blueprint/UserWidget.h"
#include "Runtime/UMG/Public/UMG.h"
#include "Stats/Stat.h"
#include "StatBarBase.generated.h"

/**
 *  A widget that shows a single stat as status bar with text overlay.
 */
UCLASS(EditInlineNew, Blueprintable, BlueprintType, Meta = (Category = "User Controls"))
class GAMEREADYAI_API UStatBarBase : public UUserWidget
{
	GENERATED_BODY()

public:
	UStatBarBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get());

	UFUNCTION(BlueprintPure, Category = "UserStats")
	UStat* GetStat() { return Stat; };

	UFUNCTION(BlueprintCallable, Category = "User Stats")
	UStatBarBase* SetStat(UStat* NewStat) { Stat = NewStat; return this; };

private:
	UPROPERTY()
	UStat* Stat;
};
