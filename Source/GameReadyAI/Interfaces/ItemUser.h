// Copyright 2015, 2016 Matthias Hölzl, All Rights Reserved.

#pragma once

#include "ItemUser.generated.h"

/**
 * An interface for actors that can use usable items. 
 */
UINTERFACE(Blueprintable, MinimalAPI)
class UItemUser : public UInterface
{
	GENERATED_UINTERFACE_BODY()
};

class IItemUser
{
	GENERATED_IINTERFACE_BODY()

public:
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Player Stats",
		Meta = (DisplayName = "Estimate Utility Of"))
	float EstimateUtilityOf(const AActor* Item) const;

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Player Stats",
		Meta = (DisplayName = "Use Item in World"))
	bool UseItemInWorld(AActor* Item);

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Player Stats",
		Meta = (DisplayName = "Add to Current Stat Value"))
	float AddToCurrentStatValue(const FName StatName, const float Delta);

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Player Stats",
		Meta = (DisplayName = "Get all Stats"))
	TArray<UStat*> GetAllStats() const;
};