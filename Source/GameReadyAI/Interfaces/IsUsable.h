// Copyright 2015, 2016 Matthias Hölzl, All Rights Reserved.

#pragma once
#include "IsUsable.generated.h"

/**
 * Interface for usable items
 */
UINTERFACE(Blueprintable, MinimalAPI)
class UIsUsable : public UInterface
{
	GENERATED_UINTERFACE_BODY()
};

class IIsUsable
{
	GENERATED_IINTERFACE_BODY()

public:
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Item",
		Meta = (DisplayName = "Estimate Item Effects"))
	float EstimateUtilityFor(const AActor* /* IItemUser */ User) const;

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Item",
		Meta = (DisplayName = "Apply Item Effects"))
	bool ApplyItemEffectsTo(AActor* /* IItemUser */ User);
};
