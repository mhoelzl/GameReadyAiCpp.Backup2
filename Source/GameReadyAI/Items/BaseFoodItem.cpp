// Copyright 2015, 2016 Matthias Hölzl, All Rights Reserved.

#include "GameReadyAI.h"
#include "AI/AIS_Character.h"
#include "BaseFoodItem.h"
#include "Stats/StatNames.h"
#include "Stats/SimpleItemEffect.h"

// Sets default values
ABaseFoodItem::ABaseFoodItem(const FObjectInitializer& ObjectInitializer /*= FObjectInitializer::Get()*/) :
	Super(ObjectInitializer)
{
	USimpleItemEffect* ItemEffect{ CreateDefaultSubobject<USimpleItemEffect>("ItemEffect") };
	ItemEffect->Initialize(StatName::Stamina, 50.0f);
	ItemEffects.Add(ItemEffect);

	PrimaryActorTick.bCanEverTick = false;

	StaticMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("StaticMesh"));
	if (RootComponent)
	{
		StaticMesh->AttachTo(RootComponent);
	}
	else
	{
		SetRootComponent(StaticMesh);
	}
}

float ABaseFoodItem::EstimateUtilityFor_Implementation(const AActor* User) const
{
	float Result{ 0.0f };
	for (UItemEffect* Effect : ItemEffects)
	{
		Result += Effect->EstimateEffect(this, User);
	}
	return Result;
}

bool ABaseFoodItem::ApplyItemEffectsTo_Implementation(AActor* User)
{
	FString UserName{ User ? User->GetName() : FString(TEXT("unnamed object")) };
	UE_LOG(LogTemp, Log, TEXT("*** %s used %s ***"), *UserName, *GetClass()->GetName());

	for (UItemEffect* Effect : ItemEffects)
	{
		Effect->ApplyEffect(this, User);
	}
	// StaticMesh->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	Destroy();
	return true;
}
