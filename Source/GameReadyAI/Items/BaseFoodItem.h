// Copyright 2015, 2016 Matthias Hölzl, All Rights Reserved.

#pragma once

#include "GameFramework/Actor.h"
#include "Interfaces/IsUsable.h"
#include "Interfaces/ItemUser.h"
#include "Stats/ItemEffect.h"
#include "BaseFoodItem.generated.h"

/**
 * Superclass for all food items.
 */
UCLASS()
class GAMEREADYAI_API ABaseFoodItem : public AActor, public IIsUsable
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	ABaseFoodItem(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get());

protected:
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Static Mesh", Meta = (AllowPrivateAccess = "true"))
	UStaticMeshComponent* StaticMesh;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Instanced, Category = "Effects", Meta = (AllowPrivateAccess = "true"))
	TArray<UItemEffect*> ItemEffects;

	// Implementation of the IsUsable interface
	virtual float EstimateUtilityFor_Implementation(const AActor* User) const override;

	virtual bool ApplyItemEffectsTo_Implementation(AActor* User) override;
};
