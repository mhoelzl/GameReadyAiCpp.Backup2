// Copyright 2015, 2016 Matthias Hölzl, All Rights Reserved.

#include "GameReadyAI.h"
#include "PlantFoodItem.h"
#include "Stats/SimpleItemEffect.h"
#include "Stats/StatNames.h"

APlantFoodItem::APlantFoodItem(const FObjectInitializer& ObjectInitializer /*= FObjectInitializer::Get()*/) :
	Super(ObjectInitializer)
{
	check(StaticMesh);
	static auto CratePath = TEXT("StaticMesh'/Game/InfinityBladeGrassLands/Environments/Misc/Exo_Deco01/StaticMesh/SM_Exo_Crate_Open_Intact.SM_Exo_Crate_Open_Intact'");
	static auto PlantPath = TEXT("StaticMesh'/Game/InfinityBladeGrassLands/Environments/Plains/Env_Plains_Flora/StaticMesh/SM_Plains_Fern04.SM_Plains_Fern04'");
	static ConstructorHelpers::FObjectFinder<UStaticMesh> MeshFinder(CratePath);
	if (MeshFinder.Succeeded())
	{
		StaticMesh->SetStaticMesh(MeshFinder.Object);
	}

	UItemEffect* HealthEffect{ CreateDefaultSubobject<USimpleItemEffect>("HealthEffect") };
	HealthEffect->Initialize(StatName::Health, 10);
	ItemEffects.Add(HealthEffect);

	UItemEffect* HappinessEffect{ CreateDefaultSubobject<USimpleItemEffect>("HappinessEffect") };
	HappinessEffect->Initialize(StatName::Happiness, -10);
	ItemEffects.Add(HappinessEffect);
}
