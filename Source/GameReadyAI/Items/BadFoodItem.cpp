// Copyright 2015, 2016 Matthias Hölzl, All Rights Reserved.

#include "GameReadyAI.h"
#include "BadFoodItem.h"
#include "Engine/EngineTypes.h"
#include "Stats/SimpleItemEffect.h"
#include "Stats/StatNames.h"

ABadFoodItem::ABadFoodItem(const FObjectInitializer& ObjectInitializer /*= FObjectInitializer::Get()*/) :
	Super(ObjectInitializer)
{
	check(StaticMesh);
	static auto MeshPath = TEXT("StaticMesh'/Game/InfinityBladeGrassLands/Environments/Plains/Env_Plains_Statues/StaticMesh/SM_Plains_Angel_Statue_01.SM_Plains_Angel_Statue_01'");
	static ConstructorHelpers::FObjectFinder<UStaticMesh> MeshFinder(MeshPath);
	if (MeshFinder.Succeeded())
	{
		StaticMesh->SetStaticMesh(MeshFinder.Object);
		StaticMesh->SetWorldScale3D(FVector(0.5));
	}

	UItemEffect* BadItemEffect{ CreateDefaultSubobject<USimpleItemEffect>("BadItemEffect") };
	BadItemEffect->Initialize(StatName::Health, -10);
	ItemEffects.Add(BadItemEffect);
}
