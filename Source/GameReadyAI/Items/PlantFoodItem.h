// Copyright 2015, 2016 Matthias Hölzl, All Rights Reserved.

#pragma once

#include "Items/BaseFoodItem.h"
#include "PlantFoodItem.generated.h"

/**
 * 
 */
UCLASS()
class GAMEREADYAI_API APlantFoodItem : public ABaseFoodItem
{
	GENERATED_BODY()
	
public:
	APlantFoodItem(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get());
};
