// Copyright 2015, 2016 Matthias Hölzl, All Rights Reserved.

#include "GameReadyAI.h"
#include "BarrelFood.h"




ABarrelFood::ABarrelFood(const FObjectInitializer& ObjectInitializer /*= FObjectInitializer::Get()*/) :
	Super(ObjectInitializer)
{
	check(StaticMesh);

	static auto Barrel = TEXT("StaticMesh'/Game/InfinityBladeGrassLands/Environments/Misc/Exo_Deco01/StaticMesh/SM_WoodenBarrel_Intact.SM_WoodenBarrel_Intact'");
	static ConstructorHelpers::FObjectFinder<UStaticMesh> MeshFinder(Barrel);
	if (MeshFinder.Succeeded())
	{
		StaticMesh->StaticMesh = MeshFinder.Object;
	}
}
