// Copyright 2015, 2016 Matthias Hölzl, All Rights Reserved.

#pragma once

#include "BehaviorTree/BTTaskNode.h"
#include "BTTask_UseItem.generated.h"

struct FBlackboardKeySelector;

/**
 * Task to use an item that implements the IsUsable interface.
 */
UCLASS()
class GAMEREADYAI_API UBTTask_UseItem : public UBTTaskNode
{
	GENERATED_BODY()
	
public:
	UBTTask_UseItem(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get());
	
	virtual void InitializeFromAsset(UBehaviorTree& Asset) override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Item")
	FBlackboardKeySelector DesiredObjectKey;

	virtual EBTNodeResult::Type ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) override;
	virtual FString GetStaticDescription() const override;
};
