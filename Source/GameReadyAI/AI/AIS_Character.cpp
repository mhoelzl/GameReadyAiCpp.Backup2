// Copyright 2015, 2016 Matthias Hölzl, All Rights Reserved.

#include "GameReadyAI.h"
#include "AI/AIS_AIController.h"
#include "AIS_Character.h"
#include "AIS_DataManager.h"
#include "Engine/EngineTypes.h"
#include "Interfaces/IsUsable.h"
#include "Kismet/KismetSystemLibrary.h"
#include "Stats/StatNames.h"
#include "UI/StatBlockBase.h"

// Sets default values
AAIS_Character::AAIS_Character(const FObjectInitializer& ObjectInitializer) :
	Super(ObjectInitializer),
	StaminaTickTime{ 0.5f },
	StaminaDecayRate{ 10.0f },
	HealRate{ 5.0f },
	HealthLossOnZeroStamina{ 5.0f },
	UseRange{ 250.f },
	StatMap()
{
	PrimaryActorTick.bCanEverTick = true;

	GetCapsuleComponent()->bGenerateOverlapEvents = false;

	InitializeSkeletalMesh();
	InitializeStatWidget();
	InitializeMovementComponent();
	InitializeAI();
}

void AAIS_Character::PostLoad()
{
	Super::PostLoad();
	InitializeStats();
}

void AAIS_Character::PostActorCreated()
{
	Super::PostActorCreated();
	InitializeStats();
}

void AAIS_Character::PostInitializeComponents()
{
	Super::PostInitializeComponents();

	check(StatWidget && StatWidget->IsValidLowLevel());
	UStatBlockBase* StatBlock{ Cast<UStatBlockBase>(StatWidget->GetUserWidgetObject()) };

	if (ensure(StatBlock && StatBlock->IsValidLowLevel()))
	{
		StatBlock->SetOwnerCharacter(this);
	}
}

#if WITH_EDITOR
void AAIS_Character::PostEditChangeProperty(struct FPropertyChangedEvent& PropertyChangedEvent)
{
	Super::PostEditChangeProperty(PropertyChangedEvent);
	UProperty* Property = PropertyChangedEvent.Property;
	if (!Property)
	{
		return;
	}
	UE_LOG(LogTemp, Log, TEXT("Changed character property %s of %s."), *Property->GetName(), *GetName());
	if (Property->GetFName() == GET_MEMBER_NAME_CHECKED(AAIS_Character, Stats))
	{
		UE_LOG(LogTemp, Log, TEXT("Updating StatMap for character."));
		StatMap.Empty();
		for (UStat* Stat : Stats)
		{
			if (Stat != nullptr)
			{
				StatMap.Add(Stat->GetStatName(), Stat);
			}
		}
	}
}
#endif

// Called when the game starts or when spawned
void AAIS_Character::BeginPlay()
{
	Super::BeginPlay();

	auto StaminaDelegate = FTimerDelegate::CreateUObject(this, &AAIS_Character::TickDownStamina);
	GetWorld()->GetTimerManager().SetTimer(StaminaTickTimer, StaminaDelegate, StaminaTickTime, true);
}

// Called every frame
void AAIS_Character::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

// Called to bind functionality to input
void AAIS_Character::SetupPlayerInputComponent(class UInputComponent* InputComponent)
{
	Super::SetupPlayerInputComponent(InputComponent);
}

float AAIS_Character::TakeDamage(float Damage, struct FDamageEvent const& DamageEvent, AController* EventInstigator,
	AActor* DamageCauser)
{
	UE_LOG(LogTemp, Log, TEXT("*** %s took damage from %s ***"),
		*GetName(),
		DamageCauser ? *DamageCauser->GetName() : TEXT("unknown actor"));
	UStat* Health{ GetStat(StatName::Health) };
	Health->AddToCurrentValue(-Damage);
	if (Health->GetCurrentValue() <= Health->GetMinValue())
	{
		AAIController* Controller{ Cast<AAIController>(GetController()) };
		if (ensure(Controller))
		{
			Controller->UnPossess();
			Controller->Destroy();
		}

		Destroy();
		Health->SetCurrentValue(Health->GetMinValue());
	}
	return Health->GetCurrentValue();
}

float AAIS_Character::EstimateUtilityOf_Implementation(const AActor* ItemToUse) const
{
	if (ItemToUse && ItemToUse->GetClass()->ImplementsInterface(UIsUsable::StaticClass()))
	{
		float Result = IIsUsable::Execute_EstimateUtilityFor(ItemToUse, this);
		return Result;
	}
	else
	{
		UE_LOG(LogTemp, Log, TEXT("*** Item %s does not implement interface IsUsable ***"), *ItemToUse->GetName());
	}
	return 0.0f;
}

bool AAIS_Character::UseItemInWorld_Implementation(AActor* ItemToUse)
{
	if (ItemToUse && ItemToUse->GetClass()->ImplementsInterface(UIsUsable::StaticClass()))
	{
		if (CanReachItem(ItemToUse))
		{
			return IIsUsable::Execute_ApplyItemEffectsTo(ItemToUse, this);
		}
		else
		{
			UE_LOG(LogTemp, Log, TEXT("*** Cannot use item %s because it is too far away ***"), *ItemToUse->GetName());
		}
	}
	else
	{
		UE_LOG(LogTemp, Log, TEXT("*** Item %s does not implement interface IsUsable ***"), *ItemToUse->GetName());
	}
	return false;
}

float AAIS_Character::AddToCurrentStatValue_Implementation(FName StatName, float Delta)
{

	UStat* Stat = GetStatOrNullptr(StatName);
	if (Stat == nullptr)
	{
		UE_LOG(LogTemp, Log, TEXT("Adding to non-existent stat value %s of actor %s."), *StatName.ToString(), *GetNameSafe(this));
		return 0.0f;
	}
	else
	{
		return Stat->AddToCurrentValue(Delta);
	}
}

TArray<UStat*> AAIS_Character::GetAllStats_Implementation(void) const
{
	return Stats;
}

void AAIS_Character::InitializeSkeletalMesh()
{
	static auto SkeletalMeshFinder{ ConstructorHelpers::FObjectFinder<USkeletalMesh>(TEXT("SkeletalMesh'/Game/Mannequin/Character/Mesh/SK_Mannequin.SK_Mannequin'")) };
	if (SkeletalMeshFinder.Succeeded())
	{
		USkeletalMeshComponent* Mesh = GetMesh();
		Mesh->SetSkeletalMesh(SkeletalMeshFinder.Object);
		Mesh->SetRelativeLocation(FVector(0.0f, 0.0f, -85.0f));
		Mesh->SetRelativeRotation(FRotator(0.0f, 270.0f, 0.0f));

		static auto AnimBPFinder{ ConstructorHelpers::FObjectFinder<UAnimBlueprintGeneratedClass>(TEXT("AnimBlueprint'/Game/Mannequin/Animations/ThirdPerson_AnimBP.ThirdPerson_AnimBP_C'")) };
		if (AnimBPFinder.Succeeded())
		{
			Mesh->SetAnimInstanceClass(AnimBPFinder.Object);
		}
	}
}

void AAIS_Character::InitializeStatWidget()
{
	StatWidget = CreateDefaultSubobject<UWidgetComponent>(TEXT("StatWidget"));
	StatWidget->SetRelativeLocation(FVector(0.0f, 0.0f, 100.0f));
	StatWidget->SetWidgetSpace(EWidgetSpace::Screen);
	StatWidget->SetDrawSize(FVector2D(250.0f, 200.0f));
	StatWidget->AttachTo(RootComponent);

	static auto StatBlockClassName{ TEXT("WidgetBlueprint'/Game/AIStream/UI/StatBlock.StatBlock_C'") };
	static auto WidgetFinder{ ConstructorHelpers::FClassFinder<UUserWidget>(StatBlockClassName) };
	if (WidgetFinder.Succeeded())
	{
		StatWidget->SetWidgetClass(WidgetFinder.Class);
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("Could not find user widget."));
	}
}

void AAIS_Character::InitializeMovementComponent()
{
	auto* MovementComponent{ GetCharacterMovement() };
	MovementComponent->MaxWalkSpeed = 400.0f;
	MovementComponent->MaxWalkSpeedCrouched = 200.0f;
	MovementComponent->MaxCustomMovementSpeed = 350.0f;
	MovementComponent->MaxAcceleration = 500.0f;

	bUseControllerRotationYaw = false;
	MovementComponent->RotationRate.Yaw = 240.0f;
	MovementComponent->bOrientRotationToMovement = true;
	MovementComponent->bUseControllerDesiredRotation = false;
}
void AAIS_Character::InitializeAI()
{
	AIControllerClass = AAIS_AIController::StaticClass();
	AutoPossessAI = EAutoPossessAI::PlacedInWorldOrSpawned;
	AutoPossessPlayer = EAutoReceiveInput::Disabled;
}

void AAIS_Character::InitializeStats()
{
	UAIS_DataManager* DataManager{ UAIS_DataManager::Get() };
	if (!ensure(DataManager))
	{
		return;
	}
	if (bAreStatsInitialized && StatMap.Num() > 0)
	{
		return;
	}
	UDataTable* StatTable{ DataManager->GetStatTable() };
	if (!ensure(StatTable != nullptr && StatTable->IsValidLowLevel()))
	{
		return;
	}
	FStreamableManager& StreamableManager{ DataManager->GetStreamableManager() };

	Stats.Empty();
	StatMap.Empty();

	TArray<FName> StatNames{ StatTable->GetRowNames() };
	for (FName StatName : StatNames)
	{
		FStatTableRow* StatData{ StatTable->FindRow<FStatTableRow>(StatName, FString(TEXT("GameMode::StatTable"))) };
		UCurveFloat* UtilityCurve = StatData->UtilityCurve.Get();
		if (!UtilityCurve)
		{
			UtilityCurve = Cast<UCurveFloat>(StreamableManager.SynchronousLoad(StatData->UtilityCurve.ToStringReference()));
		}
		ensure(UtilityCurve);

		UStat* Stat{ NewObject<UStat>(this) };
		Stat->Initialize(this, StatName, StatData);
		int32 Index{ Stats.Add(Stat) };
		StatMap.Add(StatName, Stat);

		// We set this inside the loop so that we don't set stats to initialized if we happen to receive an empty 
		// stat table.
		bAreStatsInitialized = true;
	}
}

void AAIS_Character::TickDownStamina()
{
	UStat* Stamina{ GetStat(StatName::Stamina) };
	Stamina->AddToCurrentValue(-StaminaDecayRate * StaminaTickTime);

	if (Stamina->GetCurrentValue() <= Stamina->GetMinValue())
	{
		Stamina->SetCurrentValue(Stamina->GetMinValue());
		struct FDamageEvent DamageEvent;
		TakeDamage(HealthLossOnZeroStamina, DamageEvent, GetController(), this);
	}
	else
	{
		IItemUser::Execute_AddToCurrentStatValue(this, StatName::Health, HealRate * StaminaTickTime);
	}

	UE_LOG(LogTemp, Log, TEXT("Character: %s, Stamina: %.0f, Health: %.0f"), *GetName(), Stamina->GetCurrentValue(),
		GetCurrentStatValue(StatName::Health));
}

bool AAIS_Character::CanReachItem(AActor* ItemToUse)
{
	static const TArray<AActor*> ActorsToIgnore;
	FHitResult HitResult;

	bool bHit{ UKismetSystemLibrary::LineTraceSingle_NEW(this, GetActorLocation(), ItemToUse->GetActorLocation(),  UEngineTypes::ConvertToTraceType(ECC_Visibility), false, ActorsToIgnore, EDrawDebugTrace::ForDuration, HitResult, true) };
	return bHit && (HitResult.Actor == ItemToUse) && (HitResult.Distance <= UseRange);
}


