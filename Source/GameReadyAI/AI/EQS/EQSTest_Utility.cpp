// Copyright 2016 Matthias Hölzl, All Rights Reserved.

#include "GameReadyAI.h"
#include "EQSTest_Utility.h"
#include "EnvironmentQuery/Items/EnvQueryItemType_ActorBase.h"
#include "Interfaces/IsUsable.h"
#include "Interfaces/ItemUser.h"

UEQSTest_Utility::UEQSTest_Utility(const FObjectInitializer& ObjectInitializer /*= FObjectInitializer::Get()*/) :
	Super()
{
	Cost = EEnvTestCost::Low;
	ValidItemType = UEnvQueryItemType_ActorBase::StaticClass();
}

void UEQSTest_Utility::RunTest(FEnvQueryInstance& QueryInstance) const
{
	// Some tests to ensure that running the query makes sense:
	// Obviously we need an owner.
	UObject* QueryOwner = QueryInstance.Owner.Get();
	if (QueryOwner == nullptr)
	{
		return;
	}

	// And we need an item whose utility we want to test.
	// I'm not sure whether I'm doing this the right way, since this test will basically only
	// work with contexts whose context data type is a subclass of UEnvQueryItemType_ActorBase.
	// --tc
	TArray<AActor*> UtilityContextItems;
	if (!QueryInstance.PrepareContext(UtilityContext, UtilityContextItems))
	{
		return;
	}

	FloatValueMin.BindData(QueryOwner, QueryInstance.QueryID);
	float MinThresholdValue = FloatValueMin.GetValue();

	FloatValueMax.BindData(QueryOwner, QueryInstance.QueryID);
	float MaxThresholdValue = FloatValueMax.GetValue();

	for (FEnvQueryInstance::ItemIterator It(this, QueryInstance); It; ++It)
	{
		AActor* Item{ GetItemActor(QueryInstance, It.GetIndex()) };
		if (!ensure(Item))
		{
			UE_LOG(LogTemp, Warning, TEXT("EQS Query for item utility returned invalid item"));
			continue;
		}
		for (int32 ContextIndex = 0; ContextIndex < UtilityContextItems.Num(); ++ContextIndex)
		{
			AActor* User{ UtilityContextItems[ContextIndex] };
			if (User && User->GetClass()->ImplementsInterface(UItemUser::StaticClass()))
			{
				const float Utility{ IItemUser::Execute_EstimateUtilityOf(User, Item) };
				It.SetScore(TestPurpose, FilterType, Utility, MinThresholdValue, MaxThresholdValue);
			}
			else
			{
				UE_LOG(LogTemp, Warning, TEXT("EQS Query for item utility returned invalid context (i.e., user)"));
			}
		}
	}
}
