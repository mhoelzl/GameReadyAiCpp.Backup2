// Copyright 2015 Matthias Hölzl, All Rights Reserved.

#include "GameReadyAI.h"
#include "EnvironmentQuery/Contexts/EnvQueryContext_Querier.h"
#include "EnvironmentQuery/Items/EnvQueryItemType_Point.h"
#include "EnvironmentQuery/Items/EnvQueryItemType_Actor.h"
#include "EnvironmentQuery/Generators/EnvQueryGenerator_ActorsOfClass.h"
#include "Interfaces/IsUsable.h"
#include "EQSGenerator_ActorsWithInterface.h"

#define LOCTEXT_NAMESPACE "GameReadyAI"

UEQSGenerator_ActorsWithInterface::UEQSGenerator_ActorsWithInterface(const FObjectInitializer& ObjectInitializer /*= FObjectInitializer::Get()*/)
{
	SearchCenter = UEnvQueryContext_Querier::StaticClass();
	ItemType = UEnvQueryItemType_Actor::StaticClass();
	SearchRadius.DefaultValue = 2000.0f;
	ActorBaseClass = AActor::StaticClass();
	Interface.Class = UIsUsable::StaticClass();
}

void UEQSGenerator_ActorsWithInterface::GenerateItems(FEnvQueryInstance& QueryInstance) const
{
	const UClass* InterfaceClass = Interface.GetUClass();

	if (ActorBaseClass == nullptr || InterfaceClass == nullptr)
	{
		return;
	}

	UObject* QueryOwner = QueryInstance.Owner.Get();
	if (QueryOwner == nullptr)
	{
		return;
	}

	UWorld* World = GEngine->GetWorldFromContextObject(QueryOwner);
	if (World == nullptr)
	{
		return;
	}

	SearchRadius.BindData(QueryOwner, QueryInstance.QueryID);
	const float Radius = SearchRadius.GetValue();
	const float RadiusSquared = FMath::Square(Radius);

	TArray<FVector> ContextLocations;
	QueryInstance.PrepareContext(SearchCenter, ContextLocations);

	for (TActorIterator<AActor> ItActor = TActorIterator<AActor>(World, ActorBaseClass); ItActor; ++ItActor)
	{
		if (ItActor->GetClass()->ImplementsInterface(InterfaceClass))
		{
			for (int32 ContextIndex = 0; ContextIndex < ContextLocations.Num(); ++ContextIndex)
			{
				if (FVector::DistSquared(ContextLocations[ContextIndex], ItActor->GetActorLocation()) < RadiusSquared)
				{
					QueryInstance.AddItemData<UEnvQueryItemType_Actor>(*ItActor);
					break;
				}
			}
		}
	}
}

FText UEQSGenerator_ActorsWithInterface::GetDescriptionTitle() const
{
	// FFormatNamedArguments Args;
	// Args.Add(TEXT("DescriptionTitle"), Super::GetDescriptionTitle());

	return LOCTEXT("DescriptionGenerateActorsWithInterface",
		"Actors implementing an Interface");
};

FText UEQSGenerator_ActorsWithInterface::GetDescriptionDetails() const
{
	FFormatNamedArguments Args;
	Args.Add(TEXT("InterfaceClass"), FText::FromString(GetNameSafe(Interface.GetUClass())));
	Args.Add(TEXT("DescribeContext"), UEnvQueryTypes::DescribeContext(SearchCenter));
	Args.Add(TEXT("ActorBaseClass"), FText::FromString(GetNameSafe(ActorBaseClass)));
	Args.Add(TEXT("Radius"), FText::FromString(SearchRadius.ToString()));

	FText Desc = FText::Format(LOCTEXT("ActorsWithInterfaceDescription", 
		"Base class: {ActorBaseClass}, interface: {InterfaceClass}, radius: {Radius}, center: {DescribeContext}"), Args);

	return Desc;
}

#undef LOCTEXT_NAMESPACE