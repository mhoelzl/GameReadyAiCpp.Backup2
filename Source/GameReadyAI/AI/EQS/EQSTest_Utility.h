// Copyright 2016 Matthias Hölzl, All Rights Reserved.

#pragma once

#include "EnvironmentQuery/EnvQueryTest.h"
#include "EQSTest_Utility.generated.h"

/**
 * 
 */
UCLASS()
class GAMEREADYAI_API UEQSTest_Utility : public UEnvQueryTest
{
	GENERATED_BODY()
public:
	UEQSTest_Utility(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get());

	UPROPERTY(EditDefaultsOnly, Category = "Items")
	TSubclassOf<UEnvQueryContext> UtilityContext;
	
	virtual void RunTest(FEnvQueryInstance& QueryInstance) const override;
};
